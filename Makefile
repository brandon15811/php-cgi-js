# CC = emcc
PREFIX=/media/brandon/Data41/emcc/serviceworker/php/php-7.2.8/install
CFLAGS = -O2 -c -I$(PREFIX)/include/php/ \
			-I$(PREFIX)/include/php/main \
			-I$(PREFIX)/include/php/Zend \
			-I$(PREFIX)/include/php/TSRM \
			-Wall -g


EXPORTED_FUNCTIONS = "['_main', '_eval', '_buildPhar', '_unzipCode', '_require_script']"
PRE_JS_FILE = /home/brandon/Documents/serviceworker/mock/prejs.js
PHP_FILE = /home/brandon/Documents/serviceworker/mock/test.php@/var/www/html/test.php
ZIP_FILE = /home/brandon/Documents/serviceworker/phpbb/phpBB-3.2.2.zip@/phpBB-3.2.2.zip
#EM_SHELL_FILE = /home/brandon/Documents/serviceworker/mock/hi.html
EM_SHELL_FILE = /home/brandon/Documents/serviceworker/mock/shell_minimal.html
#EMSCRIPTEN_FLAGS = -s USE_ZLIB=1 -s EXPORTED_FUNCTIONS=$(EXPORTED_FUNCTIONS) -s TOTAL_MEMORY=33554432 --pre-js $(PRE_JS_FILE) --embed-file $(CONSOLE_SCRIPT_FILE) --embed-file $(INI_FILE) --shell-file $(EM_SHELL_FILE) --minify 0
EMSCRIPTEN_FLAGS = -s EXPORTED_FUNCTIONS=$(EXPORTED_FUNCTIONS) -s TOTAL_MEMORY=128MB --pre-js $(PRE_JS_FILE) --embed-file $(PHP_FILE) --shell-file $(EM_SHELL_FILE) --minify 0 -s WASM=0 -s 'EXTRA_EXPORTED_RUNTIME_METHODS=["ccall", "cwrap", "AsciiToString"]' -s MODULARIZE=1 -s 'EXPORT_NAME="MyCode"' --preload-file $(ZIP_FILE) #--preload-file test/phpBB3/@/var/www/html/phpBB3

#OUTPUT_PREFIX=php-embed-service
OUTPUT_DIR=/home/brandon/Documents/serviceworker/mock/web
#CFILE = service-embed

#LDFLAGS = -O0 -L$(PREFIX)/lib $(PREFIX)/../libphp7.la 

#LIBTOOL = $(PREFIX)/../libtool

#all: $(CFILE).c
all:
	ln -s $(PREFIX)/bin/php-cgi $(PREFIX)/bin/php-cgi.o || true
	#TODO: Turn off debug mode
	$(CC) -O2 -o php-cgi.html $(PREFIX)/bin/php-cgi.o $(EMSCRIPTEN_FLAGS) #-s STACK_OVERFLOW_CHECK=2 #-s SAFE_HEAP=1 -s ASSERTIONS=2 
	#$(CC) -o $(OUTPUT_PREFIX).o $(CFILE).c $(CFLAGS)
	#$(LIBTOOL) --mode=link $(CC) -o $(OUTPUT_PREFIX).html $(LDFLAGS) $(EMSCRIPTEN_FLAGS) $(OUTPUT_PREFIX).o

.PHONY: install

install: all
	#rm -f $(OUTPUT_DIR)/$(OUTPUT_PREFIX).*
	#install $(OUTPUT_PREFIX).* $(OUTPUT_DIR)
	rm -f $(OUTPUT_DIR)/php-cgi.*
	install php-cgi.* $(OUTPUT_DIR)
	# sed -i "s/err('Calling stub instead of sigaction()');//" /home/brandon/Documents/serviceworker/mock/php-cgi.js
	# sed -i "s/err('Calling stub instead of signal()');//" /home/brandon/Documents/serviceworker/mock/php-cgi.js

#   Add .js so surge will gzip this file
	#mv $(OUTPUT_DIR)/$(OUTPUT_PREFIX).html.mem $(OUTPUT_DIR)/$(OUTPUT_PREFIX).html.mem.js



.PHONY: clean

clean:
	#rm -f $(OUTPUT_PREFIX).*
	rm -f php-cgi.*

