
function setEnv() {
    if (ENVIRONMENT_IS_NODE) {
        ENV = process.env;
        FS.mkdir('/home/brandon');
        FS.mkdir('/home/brandon/Documents');
        FS.mkdir('/home/brandon/Documents/serviceworker');
        FS.mkdir('/home/brandon/Documents/serviceworker/mock');
        FS.mkdir('/home/brandon/Documents/serviceworker/mock/test');
        FS.mount(NODEFS, { root: '/home/brandon/Documents/serviceworker/mock/test' }, '/home/brandon/Documents/serviceworker/mock/test');
    } else {
        ENV = Module.ENV;
        Module.arguments.push('-d', 'cgi.fix_pathinfo=0');
        Module.arguments.push('-d', 'cgi.force_redirect=0');
        FS.mkdir('/var');
        FS.mkdir('/var/www');
        FS.mkdir('/var/www/html');
        FS.mkdir('/var/www/html/phpBB3');
        
        console.log(ERRNO_CODES);

        /*BrowserFS.configure({
            fs: "ZipFS",
            options: {
                zipData: Buffer.from(Module['zipData'])
            }
        }, function (e) {
            if (e) console.error('BFS error:', e);
        });*/

        var BFS = new BrowserFS.EmscriptenFS(FS, PATH, ERRNO_CODES);
        FS.createFolder(FS.root, 'data', true, true);

        FS.mount(BFS, {root: '/phpBB3'}, '/var/www/html/phpBB3');

        // FS.mount(IDBFS, {}, '/var/www/html/phpBB3');
        // FS.syncfs(true, function (err) { if (err) console.error(err) });
    }
}
function syncFiles() {
    console.log('syncFiles');
    // FS.syncfs(false, function (err) { if (err) console.error(err) });
}
Module['preRun'].push(setEnv);
// Module['postRun'].unshift(syncFiles);