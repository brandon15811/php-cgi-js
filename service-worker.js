/*
 Copyright 2014 Google Inc. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

/// <reference path="service-worker.d.ts" />

//NOTE: getenv was causing the corruption

//TODO: Find a way to handle cookies (Send them through the main page using postMessage?)
//TODO: Move default ini path
//TODO: Compile from directory that doesn't include my name
//TODO: Add static file serving
//TODO: Prevent people from overriding actual files on the site
//TODO: Try disabling zend_try/catch
//TODO: Try compiling with debugging stuff again
//TODO: Try installing some software (like phpbb) when done
//TODO: Preload PHP so the first request doesn't take so long
//TODO: Handle index.php?
//TODO: Make a minimal build of BrowserFS?
//TODO: Fix getenv hack

/*
https://kripken.github.io/emscripten-site/docs/porting/files/packaging_files.html?highlight=preload#preloading-files
Dynamic libraries (.so): The files are precompiled and instantiated
using WebAssembly.instantiate. This is useful for browsers, such as
Chrome, that require compiling large WebAssembly modules asynchronously,
if you then want to load the module synchronously using dlopen later.
(Set Module.noWasmDecoding to true to disable).
*/

importScripts('parse-headers.js');
importScripts('https://cdnjs.cloudflare.com/ajax/libs/BrowserFS/2.0.0/browserfs.js');
// let phpModuleName = 'php-embed-service';
let phpModuleName = 'php-cgi';
importScripts(phpModuleName + '.js');
let memBlock;
let memBlockStatus;
let preloadData;

function getModule() {
  var Module = { 'preRun': [], 'postRun': [] };

  var memoryInitializer = phpModuleName + '.html.mem';
  Module['memoryInitializerRequest'] = {
    response: memBlock.slice(0),
    status: memBlockStatus
  };
  Module['getPreloadedPackage'] = function (remotePackageName, remotePackageSize) {
    console.log('Runtime asking for remote package ' + remotePackageName + ', expected size ' + remotePackageSize + 'bytes.');
    return preloadData.slice(0);
  };

  Module['zipData'] = zipData.slice(0);

  Module.noExitRuntime = false;
  //TODO: File bug that this is broken
  // Module.logReadFiles = true;

  return Module;
};

//TODO: Wait for memory init and data download before serving requests: https://stackoverflow.com/a/34680760/1951549
//  Run them during "install" phase?
async function getMem() {
  let memoryInitializer = phpModuleName + '.html.mem';
  let memRequest = await fetch(memoryInitializer);
  memBlock = await memRequest.arrayBuffer();
  memBlockStatus = memRequest.status;
}
async function getData() {
  var dataDownload = phpModuleName + '.data';
  let dataRequest = await fetch(dataDownload);
  preloadData = await dataRequest.arrayBuffer();
}
getMem();
getData();
let zipData;
async function getZip() {
  let zipRequest = await fetch('http://127.0.0.1:8001/phpBB-3.2.2.zip');
  zipData = await zipRequest.arrayBuffer();

  var Buffer = BrowserFS.BFSRequire('buffer').Buffer;
  BrowserFS.configure({
    fs: "OverlayFS",
    options: {
      readable: {
        fs: "ZipFS",
        options: {
          zipData: Buffer.from(zipData)
        }
      },
      writable: {
        fs: "InMemory",
        options: {
          storeName: 'phpbb'
        }
      }
    }
  }, function (e) {
    if (e) console.error('BFS error:', e);
  });
}
getZip();
console.log('hi');

self.addEventListener('fetch', /** @param {FetchEvent} event */ async function (event) {
  console.log('Handling fetch event for', event.request.url);
  var requestUrl = new URL(event.request.url);
  // Determine whether this is a URL Shortener API request that should be mocked.
  // Matching on just the pathname gives some flexibility in case there are multiple domains that
  // might host the same RESTful API (imagine this being used to mock responses against what might be
  // a test, or QA, or production environment).
  // Also check for the existence of the 'X-Mock-Response' header.
  //TODO: Use the pathRegex thing to fix this
  if (requestUrl.pathname.includes('.php')) {
    console.log(event);
    let Module = getModule();
    let responseBody = '';

    Module.print = (str) => {
      // console.log('stdout', str);
      responseBody += str + '\n';
    }

    Module.printErr = function (str) { console.log(str); }//(str) => console.error(str);

    const documentRoot = '/var/www/html';

    const pathRegex = /^(.+\.php)(.*)$/m;
    let pathMatch = pathRegex.exec(requestUrl.pathname);

    //Set cgi variables
    Module.ENV = {
      //TODO: Set these for post
      //CONTENT_LENGTH: '',
      //CONTENT_TYPE: '',

      DOCUMENT_ROOT: documentRoot,
      // DOCUMENT_URI: requestUrl.pathname,
      // FCGI_ROLE: 'RESPONDER',
      GATEWAY_INTERFACE: 'CGI/1.1',

      //The part after the script name
      //TODO: Only include these when pathMatch[2] exists
      //PATH_INFO: pathMatch[1],
      // PATH_TRANSLATED: documentRoot + pathMatch[2],

      //Remove "?"" from beginning of string
      QUERY_STRING: requestUrl.search.slice(1),
      REMOTE_ADDR: '127.0.0.1',
      REMOTE_PORT: '81',
      REQUEST_METHOD: event.request.method,
      //Remove ":" from end of string
      REQUEST_SCHEME: requestUrl.protocol.slice(0, -1),
      REQUEST_URI: requestUrl.pathname + requestUrl.search,
      SCRIPT_FILENAME: documentRoot + pathMatch[1],
      SCRIPT_NAME: requestUrl.pathname,

      SERVER_ADDR: '127.0.0.1',
      //TODO: Change this to the current hostname
      SERVER_NAME: '127.0.0.1',
      SERVER_PORT: requestUrl.protocol === 'https:' ? '443' : '80',
      SERVER_PROTOCOL: 'HTTP/1.1',
      SERVER_SOFTWARE: 'php-cgi.js',
    };

    console.log(Module.ENV);

    //Pass body to php-cgi if needed
    if (event.request.method !== 'GET') {
      event.request.text().then((requestBody) => {
        requestBodyIter = requestBody[Symbol.iterator]();
        //Called once for every character
        Module.stdin = () => {
          let nextChar = requestBodyIter.next();
          if (!nextChar.done) {
            return nextChar.value.charCodeAt(0);
          } else {
            return null;
          }
        }

        let contentLengthHeader = event.request.headers.get('Content-Length');
        Module.ENV['CONTENT_TYPE'] = event.request.headers.get('Content-Type').toString();
        Module.ENV['CONTENT_LENGTH'] = (contentLengthHeader ? contentLengthHeader : requestBody.length).toString();
        // Module.ENV['CONTENT_LENGTH'] = 0;
      })
    }

    // let qaz = '';
    // for (let x in Module.ENV) {
    //   qaz += `${x}=${Module.ENV[x]}\n`;
    // }
    // console.log(qaz);

    event.request.headers.forEach((value, header) => Module.ENV['HTTP_' + header.toUpperCase().replace(/-/g, '_')] = value);

    event.respondWith(new Promise((resolve, reject) => {
      const postRunFunc = () => {
        console.log('postRunFunc');
        var responseMeta = {
          // status/statusText default to 200/OK, but we're explicitly setting them here.
          status: 200,
          statusText: 'OK',
          headers: {
            // Purely optional, but we return a custom response header indicating that this is a
            // mock response. The controlled page could check for this header if it wanted to.
            'X-Mock-Response': 'yes',
          }
        };

        //TODO: Handle splitting only once (like if the body also contains \r\n\r\n)
        [headers, body] = responseBody.split('\r\n\r\n');
        Object.assign(responseMeta.headers, parseHeaders(headers));
        console.log(responseMeta.headers);

        if (responseMeta.headers.status) {
          let splitStatus = responseMeta.headers.status.split(' ');
          responseMeta.status = splitStatus.shift();
          responseMeta.statusText = splitStatus.join(' ');
          delete responseMeta.headers.status;
        }

        var mockResponse = new Response(body, responseMeta);
        console.log(mockResponse);
        resolve(mockResponse);
        delete run;
      };
      //Run this before running syncFS
      Module.postRun.push(postRunFunc);
    }));

    var run = MyCode(Module);
  }
  if (requestUrl.pathname === '/mock/hi.json' || (requestUrl.pathname === '/urlshortener/v1/url' &&
    event.request.headers.has('X-Mock-Response'))) {
    console.log('proxying');
    // This matches the result format documented at
    // https://developers.google.com/url-shortener/v1/getting_started#shorten
    var responseBody = {
      kind: 'aaa',
      id: 'http://goo.gl/IKyjuU',
      longUrl: 'https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html'
    };

    var responseMeta = {
      // status/statusText default to 200/OK, but we're explicitly setting them here.
      status: 404,
      statusText: 'OK',
      headers: {
        'Content-Type': 'application/json',
        // Purely optional, but we return a custom response header indicating that this is a
        // mock response. The controlled page could check for this header if it wanted to.
        'X-Mock-Response': 'yes'
      }
    };

    var mockResponse = new Response(JSON.stringify(responseBody), responseMeta);

    event.respondWith(new Promise((resolve) => {
      setTimeout(() => {
        console.log(' Responding with a mock response body:', responseBody);
        resolve(mockResponse);
      }, 5000);
    }));
  }

  // If event.respondWith() isn't called because this wasn't a request that we want to mock,
  // then the default request/response behavior will automatically be used.
});
