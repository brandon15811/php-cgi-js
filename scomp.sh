#!/bin/bash -ex

#TODO:
#   Fix date extension

#emconfigure ./configure  --disable-cgi --disable-short-tags --disable-ipv6 --disable-all --disable-libxml \
#	--disable-ctype --disable-inifile --disable-flatfile --disable-dom --disable-fileinfo --disable-filter \
#	--disable-hash --disable-json --disable-mbregex --disable-mbregex-backtrack --disable-opcache \
#	--disable-opcache-file --disable-huge-code-pages --disable-pdo --disable-posix --disable-session \
#	--disable-simplexml --disable-tokenizer --disable-xml --disable-xmlreader --disable-xmlwriter \
#	--disable-mysqlnd-compression-support --without-pcre-regex --without-pcre-jit --enable-phar CFLAGS=-O2 CXXFLAGS=-O2


#./buildconf --force

case "$1" in
buildconf)
    echo "Running buildconf"
    # make distclean
    ./buildconf --force
    ;&
configure)
#TODO: Remove --disable-fileinfo after patching php (maybe not because of big file size?)
#TODO: Use --disable-zend-signals?
#TODO: Test if "-fdata-sections -ffunction-sections" actually do anything
#TODO: Remove emscripten debug stuff
#TODO: Test size with sqlite3 removed
#TODO: Implement some of these https://github.com/kripken/emscripten/wiki/Chrome-Memory-Issues
##   And these https://floooh.github.io/2016/08/27/asmjs-diet.html
#TODO: Compile my own zlib?
    echo "Running configure"
    LIBXML_DIR="/home/brandon/Documents/serviceworker/xml.js/libxml2-2.9.2/install"
    ZLIB_DIR="/media/brandon/Data41/emcc/serviceworker/php/zlib/zlib-1.2.11/install"
    PERM_DISABLED_MODULES="--disable-ipv6 --without-pear --without-mysqli --disable-opcache --disable-opcache-file --disable-huge-code-pages --without-pcre-jit --disable-fileinfo --disable-sockets"
    PERM_ENABLED_MODULES="--enable-cli --enable-cgi"
    POSSIBLE_MODULES="--enable-calendar --enable-ctype --enable-exif --with-libxml-dir=${LIBXML_DIR} --enable-libxml --enable-dom --disable-simplexml --disable-xmlreader --disable-xmlwriter --with-zlib-dir=${ZLIB_DIR} --enable-zip" #--enable-phar --enable-mbstring --enable-intl
    NEEDS_COMPILE="--disable-phar"
    EMCONFIGURE_JS=1 emconfigure ./configure  $PERM_DISABLED_MODULES  $PERM_ENABLED_MODULES $NEEDS_COMPILE $POSSIBLE_MODULES --prefix="${PWD}/install/" --host=x86_64-unknown-linux-gnu CFLAGS="-fdata-sections -ffunction-sections -s -O2" CXXFLAGS="-fdata-sections -ffunction-sections -O2" LDFLAGS="-Wl,--gc-sections -Wl,-s"
    #EMCONFIGURE_JS=1 emconfigure ./configure  --enable-cgi --disable-short-tags --disable-ipv6 --disable-all --disable-libxml --disable-ctype --disable-inifile --disable-flatfile --disable-dom --disable-fileinfo --disable-filter --disable-hash --disable-mbregex --disable-mbregex-backtrack --disable-opcache --disable-opcache-file --disable-huge-code-pages --disable-pdo --disable-posix --disable-session --disable-simplexml --disable-xml --disable-xmlreader --disable-xmlwriter --disable-mysqlnd-compression-support --disable-phpdbg --disable-tokenizer --disable-json --disable-pcntl --without-pcre-regex --without-pcre-jit --disable-phar --enable-cli --disable-zip --enable-debug --prefix="${PWD}/install/" --host=x86_64-unknown-linux-gnu CFLAGS="-fdata-sections -ffunction-sections -s -O2 -s SAFE_HEAP=1 -s STACK_OVERFLOW_CHECK=2 -s ASSERTIONS=2" CXXFLAGS="-fdata-sections -ffunction-sections -O2 -s SAFE_HEAP=1 -s STACK_OVERFLOW_CHECK=2 -s ASSERTIONS=2" LDFLAGS="-Wl,--gc-sections -Wl,-s"
    #Needed for psysh: --enable-tokenizer --enable-json

    #TODO: Make PHP patches to fix these
    sed -i 's|#define HAVE_OLD_READDIR_R 1|/* #undef HAVE_OLD_READDIR_R */|' main/php_config.h
    sed -i 's|/\* #undef HAVE_UTIME \*/|#define HAVE_UTIME 1|' main/php_config.h
    sed -i 's|#define HAVE_SETITIMER 1|/* #undef HAVE_SETITIMER */|' main/php_config.h
    ;&
make)
    echo "Running make"
    emmake make clean
    emmake make -j8
    ;&
install)
    echo "Running install"
    # ln -s $PWD/libs/libphp7.{a,so}
    emmake make install
    ;&
emcc)
    echo "Running emcc"
    EMBED_PREFIX=/home/brandon/Documents/serviceworker/mock
    cd $EMBED_PREFIX
    #emmake make
    emmake make install
    ;;
*)
    echo "No option specified"
    exit 1
    ;&
esac
